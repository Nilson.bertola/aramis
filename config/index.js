const crypto = require('crypto');
const { NODE_ENV, DEV_PUBLIC_URL, PROD_PUBLIC_URL, MONGO_CONN, MONGO_PW, MONGO_USER } = process.env;

module.exports = {
  ...process.env,
  PUBLIC_URL: NODE_ENV === 'development' ? DEV_PUBLIC_URL : PROD_PUBLIC_URL,
  MONGO_STRING_CONN: MONGO_CONN.replace('<user>', MONGO_USER).replace('<password>', MONGO_PW),
  SECRET: crypto.randomBytes(1024).toString('hex'),
  AUTHORIZATION_SECRET: crypto.randomBytes(2048).toString('base64')
};
