const fs = require('fs');
const { join } = require('path');
const flattenDeep = require('lodash/flattenDeep');

const search = (ctx, match) => ctx.indexOf(match) > -1;

const loader = dirname =>
  flattenDeep(
    fs
      .readdirSync(dirname, 'utf8')
      .filter(content => !search(content, 'index.js'))
      .map(content => {
        if (!search(content, '.js')) return loader(join(dirname, content));
        return require(join(dirname, content));
      })
  );

module.exports = loader;
