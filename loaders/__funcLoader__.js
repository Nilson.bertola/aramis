const fs = require('fs');
const path = require('path');
const wontImportFiles = ['index.js', 'commons.js'];
const dot = file => file.replace('.js', '');
const join = (d, f) => path.join(d, f);
const parseImports = (key, toImport) => ({ [key]: require(`${toImport}`) });

module.exports = (dir, exception = wontImportFiles) => {
  const files = fs
    .readdirSync(dir)
    .filter(content => content.search(new RegExp('.js')))
    .filter(file => !exception.includes(file));
  const imports = files.map(f => parseImports(dot(f), join(dir, f)));
  return Object.assign({}, ...imports);
};
