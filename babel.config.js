require('dotenv').config();
require('dotenv-safe').config();

const { NODE_ENV } = require('./config');
const presets = {
  production: ['@babel/preset-env', '@babel/preset-react', 'minify'],
  development: [
    [
      '@babel/preset-env',
      {
        targets: {
          browsers: ['>0.25%', 'not ie 11', 'not op_mini all']
        }
      }
    ],
    '@babel/preset-react'
  ]
};
const plugins = [
  [require.resolve('@babel/plugin-transform-runtime')],
  [
    require.resolve('babel-plugin-module-resolver'),
    {
      root: ['./'],
      alias: {
        // APP ALIAS
        '#store': './src/app/store',
        '#components': './src/app/components',
        '#interfaces': './src/app/interfaces',
        '#assets': './src/app/assets',
        // API ALIAS
        '#loaders': './loaders',
        '#config': './config',
        '#helpers': './src/api/helpers',
        '#rest': './src/api/rest',
        '#firebase': './src/api/firebase',
        '#mongoModels': './src/api/mongo/models'
      }
    }
  ]
];
module.exports = (api, arg) => {
  api.cache(true);
  return {
    presets: presets[NODE_ENV],
    plugins,
    sourceType: 'unambiguous'
  };
};
