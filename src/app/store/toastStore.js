const _setToast = (toast, setState) =>
  setState(state => ({
    ...state,
    toast
  }));

const _clearToast = setState => setState(state => ({ ...state, ..._toastInitial }));

const _toastInitial = {
  toast: false
};

export { _setToast, _clearToast, _toastInitial };
