const _toggle = ({ param, setState }) => setState(state => ({ ...state, loading: !state.loading }));
const _loaderInitial = {
  loading: false
};

export { _toggle, _loaderInitial };
