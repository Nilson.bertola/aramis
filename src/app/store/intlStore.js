import * as locales from '../locales';
const getMessages = locale => locales[locale.replace('-', '')];
const _changeIntl = (plocale, setState) =>
  setState(state => ({
    ...state,
    ..._newIntl(plocale)
  }));
const _newIntl = plocale => ({
  intl: {
    locale: plocale,
    messages: getMessages(plocale)
  }
});
const _intlInitial = {
  intl: {
    locale: navigator.language,
    messages: getMessages(navigator.language)
  }
};

export { _changeIntl, _intlInitial };
