const _setProfile = (profile, setState) =>
  setState(state => ({
    ...state,
    profile
  }));

const _clearProfile = setState => setState(state => ({ ...state, ..._profileInitial }));

const _profileInitial = {
  profile: false
};

export { _setProfile, _clearProfile, _profileInitial };
