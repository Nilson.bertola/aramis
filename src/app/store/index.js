import React, { useContext, useMemo, useState } from 'react';
import { _changeIntl, _intlInitial } from './intlStore';
import { _loaderInitial, _toggle } from './loadStore';
import { _clearProfile, _profileInitial, _setProfile } from './profileStore';
import { _clearToast, _setToast, _toastInitial } from './toastStore';

const StoreContext = React.createContext({});

const _INITIALSTATE_ = { ..._loaderInitial, ..._intlInitial, ..._profileInitial, ..._toastInitial };

const useStoreState = () => {
  // Manage the state using React.useState()
  const [state, setState] = useState(_INITIALSTATE_);

  // Build our actions. We'll use useMemo() as an optimization,
  // so this will only ever be called once.
  const actions = useMemo(() => getActions(setState), [setState]);

  return { state, actions };
};

const getActions = setState => ({
  load: {
    toggle: param => _toggle({ param, setState })
  },
  intl: {
    changeIntl: param => _changeIntl(param, setState)
  },
  profile: {
    clear: () => _clearProfile(setState),
    set: param => _setProfile(param, setState)
  },
  toast: {
    set: param => _setToast(param, setState),
    clear: () => _clearToast(setState)
  }
});

const useStoreContext = () => useContext(StoreContext);

export { useStoreContext, StoreContext, useStoreState };
