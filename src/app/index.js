import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles.css';
import { App } from './components';

const Root = () => {
  return <App />;
};
ReactDOM.render(<Root />, document.getElementById('root'));
