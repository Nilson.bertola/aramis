import { black, whiteDark } from 'src/app/theme/colors';
import styled from 'styled-components';

export const FixedBar = styled.div`
  z-index: 5;
  position: fixed;
  height: 2rem;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: ${black};
  color: ${whiteDark};
  box-shadow: 0px -8px 8px 9px ${black};
`;

export const LinkSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  font-size: 15px;
`;

const getVisibility = pos => {
  if (pos === 'left') return 'none';
  return 'flex';
};

const getPosition = pos => {
  if (pos === 'left') return 'flex-start';
  if (pos === 'right') return 'flex-end';
  return 'center';
};

const getPositionMedia = pos => {
  if (pos === 'right') return 'flex-end';
  return 'flex-start';
};

export const MenuItem = styled.div`
  height: 2rem;
  display: flex;
  align-items: center;
  justify-content: ${({ position }) => getPosition(position)};
  position: absolute;
  width: 100%;

  @media (max-width: 768px) {
    justify-content: ${({ position }) => getPositionMedia(position)};
    display: ${({ position }) => getVisibility(position)};
  }
`;

export const Selector = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: none;
  color: #eee;
  cursor: pointer;
  padding: 8px 6px;
  margin: 2px 1px;
  border-radius: 3px;
  svg {
    margin-top: -2px;
    margin-left: 2px;
    width: 25px;
  }
`;

export const Choosen = styled.option`
  border: none;
  background: none;
`;

export const OptionsDropDown = styled.div`
  position: absolute;
  float: unset;
  z-index: 7;
  transform: translateY(-2.8rem);
`;

export const Option = styled.div`
  background-color: ${black};
  color: ${whiteDark};
  cursor: pointer;
  padding: 8px 6px;
  width: 150%;
  line-height: 1.4rem;
`;

export const IconHolderBar = styled.div`
  z-index: 2;
  svg {
    margin: 0px 7px;
    cursor: pointer;
  }
`;
