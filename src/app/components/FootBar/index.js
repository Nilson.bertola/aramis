import React, { useEffect, useState } from 'react';
import { FixedBar, MenuItem, LinkSection, IconHolderBar } from './styles';
import { LanguageSelector } from './languageSelector';
import { useStoreContext } from '#store';
import moment from 'moment';
import PropTypes from 'prop-types';
import { FA } from '#interfaces';
import { faLinkedinIn, faGithub } from '@fortawesome/free-brands-svg-icons';

const Date = ({ now }) => (
  <MenuItem position="left">
    <span>{now}</span>
  </MenuItem>
);

Date.propTypes = {
  now: PropTypes.string
};

const Owner = () => (
  <MenuItem position="center">
    <IconHolderBar>
      <FA
        icon={faLinkedinIn}
        onClick={() => window.open('https://www.linkedin.com/in/nilsonbertola/', '_blank')}
      />
      <FA icon={faGithub} onClick={() => window.open('https://github.com/NilsonBF', '_blank')} />
    </IconHolderBar>
  </MenuItem>
);

const FootBar = () => {
  const [now, setNow] = useState(moment().format('LLL'));
  const { locale } = useStoreContext().state.intl;
  useEffect(() => {
    moment.locale(locale);
    setNow(moment().format('LLL'));
  }, [locale]);
  return (
    <FixedBar>
      <LinkSection>
        <Date now={now} />
        <Owner />
        <LanguageSelector />
      </LinkSection>
    </FixedBar>
  );
};

export { FootBar };
