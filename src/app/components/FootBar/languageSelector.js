import React, { useEffect, useState, useRef } from 'react';
import { useStoreContext } from '#store';
import { MenuItem, Selector, OptionsDropDown, Option } from './styles';
import { FormattedMessage } from 'react-intl';
import US from '../../assets/images/us.svg';
import BR from '../../assets/images/br.svg';

const getLocale = loc => {
  if (loc === 'en-US') return <US />;
  if (loc === 'pt-BR') return <BR />;
};

const LanguageSelector = () => {
  const [open, setOpen] = useState(false);
  const selectRef = useRef();
  const { state, actions } = useStoreContext();
  const handleOption = e => {
    actions.intl.changeIntl(e);
    setOpen(false);
  };

  const handleClick = e => {
    if (selectRef.current && !selectRef.current.contains(e.target)) {
      setOpen(false);
      return;
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClick);
    return () => {
      document.removeEventListener('mousedown', handleClick);
    };
  }, []);

  const { locale } = state.intl;

  return (
    <div ref={selectRef}>
      <MenuItem position="right">
        <Selector onClick={() => setOpen(!open)}>
          <span>
            <FormattedMessage id="language" />
            {getLocale(locale)}
          </span>
        </Selector>

        {open && (
          <OptionsDropDown>
            <Option onClick={() => handleOption('pt-BR')}>{'Português (Brasil)'}</Option>
            <Option onClick={() => handleOption('en-US')}>{'English'}</Option>
          </OptionsDropDown>
        )}
      </MenuItem>
    </div>
  );
};

export { LanguageSelector };
