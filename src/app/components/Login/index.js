import React, { useState } from 'react';
import { EmailInput, PasswordInput } from '#interfaces';
import { LoginContainer, LoginWrapper } from './styles';
import { Button, buttonSizes, buttonTypes } from 'src/app/interfaces/Button';
import { tMessage, Regex, Toaster, fetcher } from 'src/app/tools';
import { faUserCheck } from '@fortawesome/free-solid-svg-icons';
import { useStoreContext } from '#store';
import { authorizeFetch } from 'src/app/repository';

const toastMessage = {
  wrongEmailToast: { id: 'loginEmailErrorRegex' },
  emptyPw: { id: 'loginBlankPassword' },
  authorizeErrorToast: id => ({ id })
};

const errorToastConfig = Toaster.toaster(Toaster.toasterTypes.error);

const Login = () => {
  const { toast, load, profile } = useStoreContext().actions;
  const { set } = toast;
  const { toggle } = load;
  const [email, setEmail] = useState('admin@teste.com');
  const [password, setPassword] = useState('123');
  const click = async () => {
    if (!Regex.email.test(email))
      return set({ message: toastMessage.wrongEmailToast, ...errorToastConfig });
    if (password.trim() === '') return set({ message: toastMessage.emptyPw, ...errorToastConfig });

    toggle();
    const result = await fetcher(authorizeFetch({ email, password }));
    toggle();

    if (result.status !== 200)
      return set({
        message: toastMessage.authorizeErrorToast(result.data.errCode),
        ...errorToastConfig
      });

    profile.set(result.data);
  };
  return (
    <LoginContainer>
      <LoginWrapper>
        <EmailInput value={email} setValue={setEmail} />
        <PasswordInput value={password} setValue={setPassword} />
        <Button
          size={buttonSizes.md}
          type={buttonTypes.info}
          text={tMessage('login')}
          buttonProps={{ onClick: click }}
          icon={faUserCheck}
        />
      </LoginWrapper>
    </LoginContainer>
  );
};

export { Login };
