import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { SearchInput, ListSongCards, Pagination } from '#interfaces';
import { useStoreContext } from '#store';
import { PUBLIC_URL } from '#config';

const cached = () =>
  ![null, undefined, 'null', 'undefined', ''].includes(localStorage.getItem('playlist'));

const parseCache = () => JSON.parse(localStorage.getItem('playlist'));

const DemoList = () => {
  const { toggle } = useStoreContext().actions.load;
  const [playlist, setPlaylist] = useState([]);
  const [cards, setCards] = useState([]);
  const [page, setPage] = useState(0);
  const [perPage, setPerPage] = useState(10);
  const [filter, setFilter] = useState('');
  const [filteredPlaylist, setFilteredPlaylist] = useState('');
  const [clear, setClear] = useState(false);

  useEffect(() => {
    if (cached()) {
      setPlaylist(parseCache());
    } else {
      toggle();
      axios.get(`${PUBLIC_URL}/dev/aramis/list`).then(({ data }) => {
        setPlaylist(data);
        localStorage.setItem('playlist', JSON.stringify(data));
        toggle();
      });
    }
  }, []);

  useEffect(() => {
    setFilteredPlaylist(playlist);
  }, [playlist]);

  useEffect(() => {
    if (getPages() < page) {
      setPage(getPages());
    } else {
      const initial = page * perPage;
      setCards(filteredPlaylist.slice(initial, initial + perPage));
    }
  }, [filteredPlaylist, page, perPage]);

  useEffect(() => {
    const match = new RegExp(filter, 'i');
    const filteredList = playlist.filter(
      ({ name, artist }) => match.test(name) || match.test(artist)
    );
    setFilteredPlaylist(filteredList);
    setClear(filter.trim() !== '');
  }, [filter]);

  const getPages = () => Math.floor(filteredPlaylist.length / perPage);

  return (
    <div>
      <SearchInput
        value={filter}
        onChange={e => setFilter(e.target.value)}
        clear={clear}
        clearAction={() => (clear ? setFilter('') : null)}
      />
      <Pagination
        pages={getPages()}
        page={page}
        actionPage={setPage}
        actionSelection={setPerPage}
        perPage={perPage}
      />
      <ListSongCards cards={cards} />
    </div>
  );
};

export { DemoList };
