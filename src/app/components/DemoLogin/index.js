import React, { useState } from 'react';
import { EmailInput, SearchInput, Input, PasswordInput } from '#interfaces';

const DemoLogin = () => {
  const [value, setValue] = useState('');
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        flexDirection: 'column'
      }}
    >
      <Input value={value} setValue={setValue} />
      <EmailInput value={value} setValue={setValue} />
      <SearchInput value={value} setValue={setValue} />
      <PasswordInput value={value} setValue={setValue} />
    </div>
  );
};

export { DemoLogin };
