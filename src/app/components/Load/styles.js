import { black, greenDark, orange } from 'src/app/theme/colors';
import styled, { keyframes } from 'styled-components';

export const LoaderWrapper = styled.div`
  background-color: ${black}88;
  height: 100vh;
  width: 100vw;
  justify-content: center;
  align-items: center;
  display: flex;
  position: absolute;
  z-index: 999999999;
`;

const animation = keyframes`
  to {
    transform: rotate(1turn);
  }
`;

export const Loader = styled.div`
  width: 20em;
  height: 20em;
  font-size: 10px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Face = styled.div`
  position: absolute;
  border-radius: 100%;
  border-style: double;
  animation: ${animation} 1.3s cubic-bezier(0.7, 0.55, 0.55, 0.9) infinite;
`;

export const OutOutter = styled(Face)`
  width: 90%;
  height: 90%;
  color: ${greenDark};
  border-color: currentColor transparent transparent currentColor;
  border-width: 0.2em 0.2em 0em 0em;
  --deg: -45deg;
  animation-direction: normal;
`;

export const Outter = styled(Face)`
  width: 60%;
  height: 60%;
  color: ${orange};
  border-color: currentColor currentColor transparent transparent;
  border-width: 0.2em 0em 0em 0.2em;
  --deg: -135deg;
  animation-direction: reverse;
`;

export const Circle = styled.div`
  position: absolute;
  width: 50%;
  height: 0.1em;
  top: 50%;
  left: 50%;
  background-color: transparent;
  transform: rotate(var(--deg));
  transform-origin: left;
  ::before {
    position: absolute;
    top: -0.5em;
    right: -0.5em;
    content: '';
    width: 1em;
    height: 1em;
    background-color: currentColor;
    border-radius: 50%;
    box-shadow: 0 0 2em, 0 0 4em, 0 0 6em, 0 0 8em, 0 0 10em, 0 0 0 0.5em rgba(255, 255, 255, 0.15);
  }
`;
