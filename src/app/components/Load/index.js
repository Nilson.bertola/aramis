import { useStoreContext } from '#store';
import React from 'react';
import { Circle, Loader, LoaderWrapper, OutOutter, Outter } from './styles';

const Load = () => {
  const { loading } = useStoreContext().state;
  return (
    <LoaderWrapper hidden={!loading}>
      <Loader>
        <OutOutter>
          <Circle />
        </OutOutter>
        <Outter>
          <Circle />
        </Outter>
      </Loader>
    </LoaderWrapper>
  );
};

export { Load };
