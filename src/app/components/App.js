import React, { useEffect } from 'react';
import { IntlProvider } from 'react-intl';
import { StoreContext, useStoreState } from '#store';
import Routes from './routes';
import { Load } from './Load';
import { Toast } from '#interfaces';
import { Content } from './styles';
import { TopBar } from './TopBar';
import { BrowserRouter as Router } from 'react-router-dom';
import { FootBar } from './FootBar';
import { SideBar } from './SideBar';

const App = () => {
  useEffect(() => {
    console.log();
  }, []);
  const { state, actions } = useStoreState();
  const { locale, messages } = state.intl;
  return (
    <StoreContext.Provider value={{ state, actions }}>
      <IntlProvider locale={locale} messages={messages}>
        <Router>
          <Toast />
          <Load />
          <TopBar />
          <Content>
            <SideBar />
            <Routes />
          </Content>
          <FootBar />
        </Router>
      </IntlProvider>
    </StoreContext.Provider>
  );
};

export default App;
