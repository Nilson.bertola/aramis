import { FA } from '#interfaces';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import React, { useState } from 'react';
import { Logout, Me } from './Itens';
import { Divider, MenuWrapper, MiniWrapper, Wrapper } from './styles';

const SideBar = () => {
  const [mini, setMini] = useState(true);
  return (
    <Wrapper>
      <MenuWrapper mini={mini}>
        <Me mini={mini} />
        <Divider />
        <Logout mini={mini} />
      </MenuWrapper>
      <MiniWrapper onClick={() => setMini(!mini)} mini={mini}>
        <FA icon={faAngleRight} transform={{ rotate: mini ? 0 : 180 }} />
      </MiniWrapper>
    </Wrapper>
  );
};

export { SideBar };
