import React from 'react';
import { MenuItem } from '../styles';
import { FA } from '#interfaces';
import { faIdCard } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

export const Me = ({ mini }) => (
  <MenuItem mini={mini}>
    <span>
      <FormattedMessage id="menuItemMe" />
    </span>
    <FA icon={faIdCard} size="lg" />
  </MenuItem>
);

Me.propTypes = {
  mini: PropTypes.bool.isRequired
};
