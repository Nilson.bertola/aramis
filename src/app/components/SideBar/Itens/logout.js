import React from 'react';
import { MenuItem } from '../styles';
import { FA } from '#interfaces';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

export const Logout = ({ mini }) => (
  <MenuItem mini={mini}>
    <span>
      <FormattedMessage id="logout" />
    </span>
    <FA icon={faSignOutAlt} size="lg" />
  </MenuItem>
);

Logout.propTypes = {
  mini: PropTypes.bool.isRequired
};
