import { coral, coralDark, whiteDark } from 'src/app/theme/colors';
import styled, { css, keyframes } from 'styled-components';

export const Wrapper = styled.div`
  z-index: 7;
  display: flex;
  flex-direction: row;
`;

const miniAnimation = keyframes`
  to {
    background-image: linear-gradient(to right, ${coral}, ${coralDark});
  }
`;

export const MiniWrapper = styled.div`
  width: 20px;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${coral};
  box-shadow: 8px 1px 8px -3px ${coral};
  color: ${whiteDark};
  cursor: pointer;
  @media (max-width: 768px) {
    background-image: linear-gradient(to right, ${coral}, ${coralDark});
  }
  :hover {
    animation: 0.4s ease-in-out forwards ${miniAnimation};
  }
`;
export const MenuWrapper = styled.div`
  width: ${({ mini }) => (mini ? '3rem' : '283px')};
  background-color: ${coral};
  z-index: 2;
`;

const menuAnimation = {
  div: keyframes`
    to {
      width: 283px;
      background-image: linear-gradient(to right, ${coral}, ${coralDark});
      box-shadow: 0px 0px 16px 1px ${coral};
      border-radius: 6px;
      justify-content: space-between;
    }
  `,
  span: keyframes`
    99%{
      transform: translateX(-10rem);
    }
    to {
      width: auto;
      transform: translateX(0px);
    }
  `
};

const itemMiniClosed = css`
  justify-content: center;
  justify-content: center;
  span {
    transform: translateX(-10rem);
    width: 0px;
  }
  @media (min-width: 769px) {
    :hover {
      animation: 0.7s ease-in-out forwards ${menuAnimation.div};
    }
    :hover span {
      animation: 0.7s ease-in-out forwards ${menuAnimation.span};
    }
  }
`;

const itemMiniOpen = css`
  justify-content: space-between;
`;

export const MenuItem = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  color: ${whiteDark};
  padding: 8px 0px;
  background-color: ${coral};
  cursor: pointer;

  span {
    margin: 3px;
  }
  svg {
    margin: 3px;
  }

  ${({ mini }) => (mini ? itemMiniClosed : itemMiniOpen)};
`;

export const Divider = styled.div`
  height: 1px;
  width: 100%;
  background-color: ${whiteDark};
  border-radius: 3px;
`;
