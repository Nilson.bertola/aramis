import React from 'react';
import { Paper } from '#interfaces';
import { FormattedMessage } from 'react-intl';

const Title = () => (
  <Paper.Title>
    <h1>
      <FormattedMessage id="meTitle" />
    </h1>
  </Paper.Title>
);

export { Title };
