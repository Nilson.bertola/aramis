import { FA, Paper } from '#interfaces';
import { faExternalLinkSquareAlt, faMedal } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { meFetch } from 'src/app/repository';
import { useFetch } from 'src/app/tools';
import { Divider, Image, ImageSection, PremiumRow, ProfileSection, Row } from './styles';
import { Title } from './title';

const Me = () => {
  const data = useFetch(meFetch);
  if (data.response) {
    const { name, img, email, href, premium, totalFollowers } = data.response;
    return (
      <Paper.Main elevateOnHover={true}>
        <Title />
        <Paper.Content>
          <ImageSection>
            <Image src={img} />
          </ImageSection>
          <Divider />
          <ProfileSection>
            <Row>
              <h2>{name}</h2>
            </Row>
            <Row>
              <h6>{email}</h6>
            </Row>
            <Row>
              <h6>
                <FormattedMessage id="meFollowers" values={{ num: totalFollowers }} />
              </h6>
            </Row>
            {premium && (
              <PremiumRow>
                <FA icon={faMedal} />
                <FormattedMessage id="mePremium" />
              </PremiumRow>
            )}
          </ProfileSection>
        </Paper.Content>
        <Paper.Actions>
          <div>
            <FA icon={faExternalLinkSquareAlt} />
          </div>
        </Paper.Actions>
      </Paper.Main>
    );
  }

  return <div></div>;
};

export { Me };
