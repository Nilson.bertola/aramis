import { black, blackLight, greenDark, greyLight2 } from 'src/app/theme/colors';
import styled from 'styled-components';

export const HomeWrapper = styled.div`
  margin: 6px 3px;
  padding: 6px 3px;
  box-shadow: 0px 0px 7px ${greyLight2};
`;

export const Title = styled.h1`
  font-family: 'Carter One', cursive;
  margin: 6px 0px;
`;

export const Main = styled.div`
  display: flex;
  height: auto;
  flex-flow: row column;
`;

export const ProfileSection = styled.div`
  display: flex;
  justify-content: center;
  flex-flow: column;
`;

export const ImageSection = styled.div.attrs({
  className: 'col-3'
})`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Image = styled.img`
  object-fit: contain;
  width: 100%;
`;

export const Divider = styled.div`
  width: 1px;
  border-radius: 5px;
  background-image: radial-gradient(circle, ${black}, ${blackLight}, transparent, transparent);
`;

export const Row = styled.div`
  margin: 6px;
`;

export const PremiumRow = styled(Row)`
  font-family: 'Carter One', cursive;
  color: ${greenDark};
`;

export const ButtonRow = styled(Row)`
  display: flex;
  flex-direction: row;
  align-content: center;
  justify-content: flex-end;
`;
