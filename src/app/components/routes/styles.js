import styled from 'styled-components';
import { black } from '../../theme/colors';

export const Container = styled.div.attrs({
  className: 'container'
})`
  color: ${black};
  @media (max-width: 768px) {
    width: 100%;
    max-width: none;
  }
`;

export const Root = styled.div`
  padding: 17px 0;
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;
