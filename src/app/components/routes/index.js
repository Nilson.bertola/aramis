import { useStoreContext } from '#store';
import PropTypes from 'prop-types';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Home } from '../Home';
import { Login } from '../Login';
import { Me } from '../Me';
// import { DemoList } from '#components/DemoList';
// import { DemoLogin } from '#components/DemoLogin';
import { Container, Root } from './styles';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { profile } = useStoreContext().state;
  return (
    <Route
      {...rest}
      render={props =>
        profile ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        )
      }
    />
  );
};

const LoginRoute = ({ component: Component, ...rest }) => {
  const { profile } = useStoreContext().state;
  return (
    <Route
      {...rest}
      render={props =>
        profile === false ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/me', state: { from: props.location } }} />
        )
      }
    />
  );
};

const Routes = props => {
  return (
    <Root>
      <Container>
        <Switch>
          <Route path="/" exact component={Home} />
          <LoginRoute path="/login" exact component={Login} />
          <PrivateRoute path="/me" exact component={Me} />
        </Switch>
      </Container>
    </Root>
  );
};

export default Routes;

PrivateRoute.propTypes = {
  component: PropTypes.func,
  location: PropTypes.any
};

LoginRoute.propTypes = {
  component: PropTypes.func,
  location: PropTypes.any
};
