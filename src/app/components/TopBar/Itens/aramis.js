import robot from '#assets/images/robot.png';
import PropTypes from 'prop-types';
import React from 'react';
import { MenuItem } from '../styles';

export const Aramis = ({ goTo }) => (
  <MenuItem onClick={() => goTo('/')}>
    <img src={robot} />
  </MenuItem>
);

Aramis.propTypes = {
  goTo: PropTypes.func.isRequired
};
