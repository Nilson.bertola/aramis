import PropTypes from 'prop-types';
import React from 'react';
import { MenuItem } from '../styles';
import { faHeadphones } from '@fortawesome/free-solid-svg-icons';
import { FA } from '#interfaces';

export const Greet = ({ profile }) => (
  <MenuItem greet={true}>
    <FA icon={faHeadphones} size="xs" />
    &nbsp;
    {profile.fName && <span>{profile.fName}</span>}
    {!profile.fName && <span>{profile.email}</span>}
  </MenuItem>
);

Greet.propTypes = {
  profile: PropTypes.any
};
