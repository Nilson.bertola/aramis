import PropTypes from 'prop-types';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { MenuItem } from '../styles';

export const Login = ({ goTo }) => (
  <MenuItem onClick={() => goTo('/login')}>
    <span>
      <FormattedMessage id="login" />
    </span>
  </MenuItem>
);
Login.propTypes = {
  goTo: PropTypes.func.isRequired
};
