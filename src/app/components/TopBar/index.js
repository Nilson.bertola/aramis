import React from 'react';
import { FixedBar, LinkSection } from './styles';
import { withRouter } from 'react-router-dom';
import { Login, Aramis, Greet } from './Itens';
import { useStoreContext } from '#store';

const TopBar = withRouter(({ history }) => {
  const { profile } = useStoreContext().state;
  const goTo = path => {
    history.push(path);
  };
  return (
    <FixedBar>
      <LinkSection>
        <Aramis goTo={goTo} />
      </LinkSection>
      <LinkSection>
        {!profile && <Login goTo={goTo} />}
        {profile && <Greet profile={profile} />}
      </LinkSection>
    </FixedBar>
  );
});

export { TopBar };
