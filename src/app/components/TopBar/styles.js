import { blackLight, whiteDark } from 'src/app/theme/colors';
import styled, { css } from 'styled-components';

const condicionalSidebar = css`
  box-shadow: 0px 8px 8px 9px ${blackLight};
`;

export const FixedBar = styled.div`
  left: 0;
  right: 0;
  top: 0;
  position: fixed;
  height: 5.5rem;
  z-index: 5;
  background-color: ${blackLight};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  ${condicionalSidebar}
`;

export const LinkSection = styled.div`
  display: flex;
  flex-direction: row;
`;

export const MenuItem = styled.div`
  height: 5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${whiteDark};
  ${({ greet }) => (greet ? '' : 'cursor: pointer;')}

  span {
    font-family: 'Carter One', cursive;
  }

  :nth-child(n) {
    border-bottom: 1px solid ${whiteDark}77;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
    margin: 0px 10px;
  }

  img {
    height: 100%;
  }
`;
