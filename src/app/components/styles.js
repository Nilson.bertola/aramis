import styled from 'styled-components';

export const Content = styled.div`
  margin-top: 5.5rem;
  margin-bottom: 2rem;
  flex: 1;
  display: flex;
`;
