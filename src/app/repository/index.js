import { PUBLIC_URL, AUTHORIZATION_SECRET } from '#config';
const Authorization = `Basic ${AUTHORIZATION_SECRET}`;

export const meFetch = {
  url: `${PUBLIC_URL}/dev/aramis/me`,
  method: 'GET',
  headers: {
    Authorization
  }
};

export const authorizeFetch = data => ({
  url: `${PUBLIC_URL}/dev/auth/authorize`,
  method: 'POST',
  data,
  headers: {
    Authorization
  }
});
