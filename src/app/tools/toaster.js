import {
  faBell,
  faCertificate,
  faCheckCircle,
  faExclamationTriangle,
  faInfoCircle,
  faTimesCircle
} from '@fortawesome/free-solid-svg-icons';

const parseToastType = {
  info: {
    type: 'info',
    icon: faInfoCircle,
    canClose: true
  },
  success: {
    type: 'success',
    icon: faCheckCircle,
    canClose: true
  },
  danger: {
    type: 'danger',
    icon: faCertificate,
    canClose: false
  },
  error: {
    type: 'error',
    icon: faTimesCircle,
    canClose: false
  },
  alert: {
    type: 'alert',
    icon: faExclamationTriangle,
    canClose: true
  },
  dark: {
    type: 'dark',
    icon: faBell,
    canClose: true
  },
  [null]: {
    type: '',
    icon: false,
    canClose: true
  }
};

const toaster = (type = null) => parseToastType[type];

const toasterTypes = {
  info: 'info',
  success: 'success',
  danger: 'danger',
  error: 'error',
  alert: 'alert',
  dark: 'dark',
  null: 'null'
};

const Toaster = { toaster, toasterTypes };
export default Toaster;
