import { useState, useEffect } from 'react';
import axios from 'axios';
import { useStoreContext } from '#store';

const useFetch = options => {
  const { toggle } = useStoreContext().actions.load;
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const fetchAPI = async () => {
    toggle();
    try {
      const { data } = await axios(options);
      setResponse(data);
    } catch (e) {
      setError(e);
    }
    toggle();
  };

  useEffect(() => {
    fetchAPI();
  }, []);
  return { response, error };
};

export { useFetch };
