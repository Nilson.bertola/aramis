import { useIntl } from 'react-intl';
export const tMessage = id => useIntl().formatMessage({ id });
