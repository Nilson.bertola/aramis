import axios from 'axios';

export const fetcher = options =>
  new Promise(resolve =>
    axios(options)
      .then(o => resolve(o))
      .catch(e => resolve(e.response))
  );
