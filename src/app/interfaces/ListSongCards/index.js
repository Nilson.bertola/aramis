import React from 'react';
import { ListCards } from './styles';
import { SongCard } from '../SongCard';

const ListSongCards = ({ cards, ...props }) => (
  <ListCards>
    {cards.length > 0 && cards.map((card, i) => <SongCard {...card} key={i} />)}
  </ListCards>
);

export { ListSongCards };
