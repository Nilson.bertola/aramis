import styled from 'styled-components';

export const ListCards = styled.div.attrs({
  classNames: ''
})`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: stretch;
`;
