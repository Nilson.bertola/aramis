import styled, { keyframes } from 'styled-components';
import {
  blueLight2,
  green,
  coral,
  redLight,
  yellow,
  black,
  greyDark1,
  blueDark1,
  greenDark,
  coralDark,
  red,
  orange,
  blackLight,
  greyDark2,
  whiteDark
} from 'src/app/theme/colors';

const getToastColors = color => {
  if (color === 'info') return blueLight2;
  if (color === 'success') return green;
  if (color === 'danger') return coral;
  if (color === 'error') return redLight;
  if (color === 'alert') return yellow;
  if (color === 'dark') return blackLight;
  return greyDark1;
};

const getToastBorder = color => {
  if (color === 'info') return blueDark1;
  if (color === 'success') return greenDark;
  if (color === 'danger') return coralDark;
  if (color === 'error') return red;
  if (color === 'alert') return orange;
  if (color === 'dark') return black;
  return greyDark2;
};

const getToastFontColor = color => {
  if (['success', 'danger', 'error', 'alert'].includes(color)) return black;
  if (['info', 'dark'].includes(color)) return whiteDark;
  return black;
};

const slideIn = keyframes`
    70%{transform: translateY(90px);}
    80%{transform: translateY(40px);}
    90%{transform: translateY(75px);}
    95%{transform: translateY(45px);}
    100% {
        transform: translateY(65px);
    }
`;
const slideOut = keyframes`
    100% {
      transform: translateY(0px);
    }
`;

export const ToastContainer = styled.div`
  top: -50px;
  justify-self: center;
  position: absolute;
  align-items: center;
  background-color: ${({ type }) => getToastColors(type)};
  border-radius: 4px;
  border: 1px solid ${({ type }) => getToastBorder(type)};
  color: ${({ type }) => getToastFontColor(type)};
  display: flex;
  flex-flow: row column;
  flex-wrap: wrap;
  flex: 1;
  justify-content: center;
  margin: 11px 4px;
  min-width: 54px;
  overflow-wrap: break-word;
  padding: 6px 8px;
  word-wrap: break-word;
  animation: ${({ toggle }) => (toggle ? slideIn : slideOut)} 1s cubic-bezier(1, 0, 0, 1) forwards;
  box-shadow: 0px 0px 17px ${({ type }) => getToastBorder(type)};

  span {
    margin: 0px 10px;
    font-family: 'Carter One', cursive;
  }

  div {
    opacity: 0.6;
    cursor: pointer;
  }
`;

export const ToastWrapper = styled.div`
  background-color: transparent;
  height: 0px;
  width: 100vw;
  justify-content: center;
  align-items: center;
  display: flex;
  position: absolute;
  z-index: 999999999;
`;
