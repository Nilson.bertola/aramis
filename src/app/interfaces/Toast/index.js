import React, { useEffect } from 'react';
import { FA } from '../FA';
import { ToastContainer, ToastWrapper } from './styles';
import { useStoreContext } from '#store';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { FormattedMessage } from 'react-intl';

const Toast = () => {
  const { toast } = useStoreContext().state;
  const { clear } = useStoreContext().actions.toast;
  useEffect(() => {
    if (toast !== false)
      setTimeout(() => {
        clear();
      }, 7000);
  }, [toast]);
  return (
    <ToastWrapper>
      <ToastContainer toggle={toast !== false} type={toast.type}>
        {toast.icon && <FA icon={toast.icon} />}
        {toast.message && (
          <span>
            <FormattedMessage {...toast.message} />
          </span>
        )}
        {toast.canClose && (
          <div>
            <FA icon={faWindowClose} onClick={toast.canClose ? clear : null} />
          </div>
        )}
      </ToastContainer>
    </ToastWrapper>
  );
};

export { Toast };
