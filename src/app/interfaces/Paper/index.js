import { Main } from './main';
import { Title } from './title';
import { Content } from './content';
import { Actions } from './actions';

const Paper = {
  Main,
  Title,
  Content,
  Actions
};

export { Paper };
