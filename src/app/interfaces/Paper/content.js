import React from 'react';
import PropTypes from 'prop-types';
import { ContentWrapper } from './styles';

const Content = ({ children }) => <ContentWrapper>{children}</ContentWrapper>;

export { Content };

Content.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element])
};
