import { greyLight1, whiteDark, blackLight } from 'src/app/theme/colors';
import styled, { keyframes, css } from 'styled-components';

const elevateOnHoverAnimation = keyframes`
  to{
    box-shadow: 0px 0px 24px ${blackLight};
  }
`;

const elevateOnHoverCss = css`
  :hover {
    animation: ${elevateOnHoverAnimation} 0.3s forwards ease-in-out;
  }
`;

const getAnimations = ({ elevateOnHover }) => {
  if (elevateOnHover) return elevateOnHoverCss;
};

export const MainWrapper = styled.div.attrs({
  className: 'col-12'
})`
  border-radius: 4px;
  padding: 6px 3px;
  background-color: ${whiteDark};
  box-shadow: 0px 0px 12px ${greyLight1};
  ${getAnimations}
`;

export const TitleWrapper = styled.div.attrs({
  className: 'col-12'
})`
  font-family: 'Carter One', cursive;
  padding: 6px;
  span,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p {
    font-family: 'Carter One', cursive;
  }
`;

export const ContentWrapper = styled.div`
  display: flex;
  height: auto;
  flex-flow: row column;
`;

export const ActionWrapper = styled.div`
  padding: 6px;
  display: flex;
  flex-direction: row;
  align-content: center;
  justify-content: flex-end;
`;
