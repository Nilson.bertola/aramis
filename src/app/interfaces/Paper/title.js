import React from 'react';
import PropTypes from 'prop-types';
import { TitleWrapper } from './styles';

const Title = ({ children }) => <TitleWrapper>{children}</TitleWrapper>;

export { Title };

Title.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element])
};
