import React from 'react';
import PropTypes from 'prop-types';
import { MainWrapper } from './styles';

const Main = ({ children, elevateOnHover = false }) => (
  <MainWrapper elevateOnHover={elevateOnHover}>{children}</MainWrapper>
);

export { Main };

Main.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element]),
  elevateOnHover: PropTypes.bool
};
