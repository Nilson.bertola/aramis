import React from 'react';
import PropTypes from 'prop-types';
import { ActionWrapper } from './styles';

const Actions = ({ children }) => <ActionWrapper>{children}</ActionWrapper>;

export { Actions };

Actions.propTypes = {
  children: PropTypes.element
};
