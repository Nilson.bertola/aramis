import React from 'react';
import PropTypes from 'prop-types';
import { BaseInput } from './baseInput';

const Input = ({ value, setValue, ...rest }) => (
  <BaseInput value={value} setValue={e => setValue(e.target.value)} />
);
export { Input };

Input.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  setValue: PropTypes.func.isRequired
};
