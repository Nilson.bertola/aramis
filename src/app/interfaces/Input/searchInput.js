import React from 'react';
import PropTypes from 'prop-types';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { BaseInput } from './baseInput';
import { tMessage } from 'src/app/tools';

const SearchInput = ({ value, setValue, ...rest }) => {
  const clearValue = () => setValue('');
  return (
    <BaseInput
      placeholder={tMessage('inputSearch')}
      value={value}
      setValue={e => setValue(e.target.value)}
      setClear={clearValue}
      pIcon={faSearch}
    />
  );
};

export { SearchInput };

SearchInput.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  setValue: PropTypes.func.isRequired
};
