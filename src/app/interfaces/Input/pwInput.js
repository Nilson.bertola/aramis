import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { BaseInput } from './baseInput';
import { tMessage } from 'src/app/tools';

const PasswordInput = ({ value, setValue, ...rest }) => {
  const [hide, setHide] = useState(true);
  return (
    <BaseInput
      placeholder={tMessage('inputPassword')}
      value={value}
      setValue={e => setValue(e.target.value)}
      type={hide ? 'password' : 'text'}
      tooglePwScurity={() => setHide(!hide)}
    />
  );
};
export { PasswordInput };

PasswordInput.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  setValue: PropTypes.func.isRequired
};
