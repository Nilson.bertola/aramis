import { blueLight2, greyLight2, orange, red, redLight } from 'src/app/theme/colors';
import styled, { css, keyframes } from 'styled-components';

const iconAnimation = keyframes`
  to{
    color: ${red};
  }
`;

const pwIconHiden = keyframes`
  to{
    color: ${orange};
  }
`;

const pwIconShown = keyframes`
  to{
    color: ${blueLight2};
  }
`;

const iconOnHover = css`
  cursor: pointer;
  :hover {
    animation: 0.5s ${iconAnimation} ease-in-out forwards;
  }
`;

const pwIconHidenHover = css`
  cursor: pointer;
  :hover {
    animation: 0.5s ${pwIconHiden} ease-in-out forwards;
  }
`;

const pwIconShownHover = css`
  cursor: pointer;
  :hover {
    animation: 0.5s ${pwIconShown} ease-in-out forwards;
  }
`;

const errorStyle = css`
  border: 1px dashed ${red};
  border-radius: 24px;
  background-color: ${redLight};
`;

const style = css`
  border: 1px solid ${greyLight2};
  border-radius: 24px;
`;

const animationLogic = ({ hide, clear, tooglePwScurity }) => {
  if (!tooglePwScurity && !hide && !clear) return '';
  if (!hide && clear) return iconOnHover;
  if (!clear && tooglePwScurity && hide) return pwIconHidenHover;
  if (!clear && tooglePwScurity && !hide) return pwIconShownHover;
};

export const InputMain = styled.div`
  margin: 5px 0px;
  border: 1px solid ${greyLight2};
  border-radius: 24px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  min-height: 44px;
  ${({ error }) => (error ? errorStyle : style)}
`;

export const ElementHolder = styled.div`
  padding: 5px 5px 2px 10px;
  display: flex;
  flex: 1;
`;

export const InputElement = styled.input`
  background-color: transparent;
  border: none;
  font-family: 'Carter One', cursive;
  :focus {
    outline: none;
  }
`;

export const IconHolder = styled(ElementHolder)`
  ${({ ...props }) => animationLogic(props)}
  svg {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin: 5px;
  }
`;
