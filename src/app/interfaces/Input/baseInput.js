import React from 'react';
import PropTypes from 'prop-types';
import { InputMain, InputElement, ElementHolder, IconHolder } from './styles';
import { faBackspace, faKeyboard, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { FA } from '../FA';

const isEmpty = val => [null, undefined, ''].includes(val);
const isHide = val => val === 'password';

const BaseInput = ({
  value,
  setValue,
  placeholder = '',
  setClear = false,
  pIcon = faKeyboard,
  error = false,
  type = 'text',
  tooglePwScurity = false
}) => (
  <InputMain error={error}>
    <ElementHolder>
      <InputElement value={value} onChange={setValue} placeholder={placeholder} type={type} />
    </ElementHolder>
    {setClear && (
      <IconHolder clear={!isEmpty(value)} onClick={!isEmpty(value) ? setClear : undefined}>
        <FA icon={isEmpty(value) ? pIcon : faBackspace} size="lg" />
      </IconHolder>
    )}
    {tooglePwScurity && (
      <IconHolder hide={isHide(type)} tooglePwScurity={tooglePwScurity} onClick={tooglePwScurity}>
        <FA icon={isHide(type) ? faEye : faEyeSlash} size="lg" />
      </IconHolder>
    )}
  </InputMain>
);

export { BaseInput };

BaseInput.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  setValue: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  pIcon: PropTypes.any,
  error: PropTypes.bool,
  type: PropTypes.string,
  setClear: PropTypes.func,
  tooglePwScurity: PropTypes.func
};
