import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import { faAt } from '@fortawesome/free-solid-svg-icons';
import { BaseInput } from './baseInput';

const regex = new RegExp(/\w+@\w+\.\w+/);

const placeholder = { id: 'inputEmail' };

const EmailInput = ({ value, setValue, ...rest }) => {
  const [error, setError] = useState(false);
  useEffect(() => {
    if (value === '') {
      setError(false);
    } else {
      setError(!regex.test(value));
    }
  }, [value]);

  const clearValue = () => setValue('');
  const intl = useIntl();

  return (
    <BaseInput
      placeholder={intl.formatMessage(placeholder)}
      value={value}
      setValue={e => setValue(e.target.value)}
      setClear={clearValue}
      pIcon={faAt}
      error={error}
    />
  );
};

export { EmailInput };

EmailInput.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  setValue: PropTypes.func.isRequired
};
