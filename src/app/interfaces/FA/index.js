import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const FA = ({ ...props }) => <FontAwesomeIcon {...props} />;

export { FA };
