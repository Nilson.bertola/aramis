import styled, { keyframes } from 'styled-components';

const hoverCardAnimation = keyframes`
  0% {
    box-shadow: 3px 3px 20px #737373;
  }
  100% {
    box-shadow: 5px 5px 30px #333;
  }
`;

export const Main = styled.div.attrs({
  className: 'col-sm-5 row'
})`
  margin: 15px;
  border-radius: 5px;
  box-shadow: 0.5px 0.5px 5px #737373;

  &:hover {
    animation: ${hoverCardAnimation} 0.3s forwards ease-in-out;
  }
`;

export const AlbumImage = styled.div.attrs({
  className: 'col-5'
})`
  display: flex;
  justify-content: center;
  align-items: center;
  img {
    margin: 4px;
    box-shadow: 1px 1px 10px #737373;
    border-radius: 5px;
    width: 100%;
    object-fit: fill;
  }
`;
