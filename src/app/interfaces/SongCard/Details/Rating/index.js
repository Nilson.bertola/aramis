import React from 'react';
import { faStar as emptyStar } from '@fortawesome/free-regular-svg-icons';
import { faStar as fullStart, faStarHalfAlt as halfStar } from '@fortawesome/free-solid-svg-icons';
import { StarRow } from './styles';
import { FA } from 'src/app/interfaces/FA';

const zero = () => <FA icon={emptyStar} />;

const half = () => <FA icon={halfStar} />;

const one = () => <FA icon={fullStart} />;

const oneAndHalf = () => (
  <StarRow>
    <FA icon={fullStart} />
    <FA icon={halfStar} />
  </StarRow>
);

const two = () => (
  <StarRow>
    <FA icon={fullStart} />
    <FA icon={fullStart} />
  </StarRow>
);

const twoAndHalf = () => (
  <StarRow>
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={halfStar} />
  </StarRow>
);

const three = () => (
  <StarRow>
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={fullStart} />
  </StarRow>
);

const threeAndHalf = () => (
  <StarRow>
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={halfStar} />
  </StarRow>
);

const four = () => (
  <StarRow>
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={fullStart} />
  </StarRow>
);

const fourAndHalf = () => (
  <StarRow>
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={halfStar} />
  </StarRow>
);

const five = () => (
  <StarRow>
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={fullStart} />
    <FA icon={fullStart} />
  </StarRow>
);

const Stars = ({ popularity }) => {
  if (popularity === 0) return zero();
  if (popularity <= 10) return half();
  if (popularity <= 20) return one();
  if (popularity <= 30) return oneAndHalf();
  if (popularity <= 40) return two();
  if (popularity <= 50) return twoAndHalf();
  if (popularity <= 60) return three();
  if (popularity <= 70) return threeAndHalf();
  if (popularity <= 80) return four();
  if (popularity <= 90) return fourAndHalf();
  return five();
};

const Rating = ({ popularity, hover, exitHover }) => (
  <StarRow onMouseEnter={hover} onMouseLeave={exitHover}>
    <Stars popularity={popularity} />
  </StarRow>
);

export { Rating };
