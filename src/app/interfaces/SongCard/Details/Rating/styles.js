import styled from 'styled-components';

export const StarRow = styled.div`
  display: flex;
  flex-direction: row;
  width: min-content;

  svg {
    color: #f4ae01;
  }
`;
