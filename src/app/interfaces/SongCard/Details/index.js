import React, { useState } from 'react';
import { Main, Music, Artist, Buttons, Row } from './styles';
import { FA } from '../../FA';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';
import { Rating } from './Rating';
import { Tooltip } from './Tooltip';

const renderMusic = music => (
  <Row>
    <Music>{music}</Music>
  </Row>
);

const renderArtists = artists => (
  <Row>
    <Artist>{artists}</Artist>
  </Row>
);

const goToLink = url => window.open(url, '_blank');

const Details = ({ href, name, popularity, artist }) => {
  const [tooltip, setTooltip] = useState(false);

  const renderPopularity = () => (
    <Row>
      {tooltip && <Tooltip popularity={popularity} />}
      <Rating
        popularity={popularity}
        hover={() => setTooltip(true)}
        exitHover={() => setTooltip(false)}
      />
    </Row>
  );

  return (
    <Main>
      {renderMusic(name)}
      {renderArtists(artist)}
      {renderPopularity()}
      <Buttons>
        <FA icon={faExternalLinkAlt} onClick={() => goToLink(href)} />
      </Buttons>
    </Main>
  );
};

export { Details };
