import styled from 'styled-components';

export const TooltipWrapper = styled.div`
  position: absolute;
  float: unset;
  z-index: 2;
  margin-top: 19px;
`;
