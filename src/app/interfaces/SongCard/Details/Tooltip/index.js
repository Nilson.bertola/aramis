import React from 'react';
import { TooltipWrapper } from './styles';

const Tooltip = ({ popularity }) => <TooltipWrapper>{popularity}/100</TooltipWrapper>;

export { Tooltip };
