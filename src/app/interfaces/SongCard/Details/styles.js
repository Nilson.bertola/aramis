import styled from 'styled-components';

export const Main = styled.div.attrs({
  className: 'col-7'
})`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-direction: column;
`;

export const Row = styled.div`
  margin: 5px 0px;
  width: 100%;
`;

export const Music = styled.span`
  font-size: ;
`;

export const Artist = styled.span`
  font-size: ;
`;

export const Popularity = styled.span`
  font-size: ;
`;

export const Buttons = styled(Row)`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  flex-direction: column;

  div,
  svg {
    cursor: pointer;
  }
`;
