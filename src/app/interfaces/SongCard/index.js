import React from 'react';
import { AlbumImage, Main } from './styles';
import { Details } from './Details';

const SongCard = ({ img, ...props }) => (
  <Main>
    <AlbumImage>
      <img src={img} />
    </AlbumImage>
    <Details {...props} />
  </Main>
);

export { SongCard };
