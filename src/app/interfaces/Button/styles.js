import {
  black,
  blackLight,
  blueDark1,
  blueLight2,
  coral,
  coralDark,
  green,
  greenDark,
  greyDark1,
  greyDark2,
  orange,
  red,
  redLight,
  whiteDark,
  yellow
} from 'src/app/theme/colors';
import styled, { keyframes } from 'styled-components';

const getButtonSize = size => {
  if (size === 'sm') return 'col-3';
  if (size === 'md') return 'col-6';
  if (size === 'lg') return 'col-12';
  return 'col-6';
};

const getButtonColors = color => {
  if (color === 'info') return blueLight2;
  if (color === 'success') return green;
  if (color === 'danger') return coral;
  if (color === 'error') return redLight;
  if (color === 'alert') return yellow;
  if (color === 'dark') return blackLight;
  return greyDark1;
};

const getButtonBorder = color => {
  if (color === 'info') return blueDark1;
  if (color === 'success') return greenDark;
  if (color === 'danger') return coralDark;
  if (color === 'error') return red;
  if (color === 'alert') return orange;
  if (color === 'dark') return black;
  return greyDark2;
};

const getButtonFontColor = color => {
  if (['success', 'danger', 'error', 'alert'].includes(color)) return black;
  if (['info', 'dark'].includes(color)) return whiteDark;
  return black;
};

const getButtonKeyframe = color => keyframes`
  to {
    box-shadow: 0px 0px 13px ${getButtonBorder(color)};
  }
`;

export const ButtonContainer = styled.div.attrs(({ size = '' }) => ({
  className: getButtonSize(size)
}))`
  cursor: pointer;
  display: flex;
  flex-flow: row column;
  justify-content: center;
  align-items: center;
  background-color: ${({ type }) => getButtonColors(type)};
  border: 1px solid ${({ type }) => getButtonBorder(type)};
  margin: 11px 4px;
  min-width: 54px;
  padding: 6px 8px;
  border-radius: 4px;
  color: ${({ type }) => getButtonFontColor(type)};
  flex-wrap: wrap;
  overflow-wrap: break-word;
  word-wrap: break-word;
  :hover {
    animation: 0.3s forwards ease-in-out ${({ type }) => getButtonKeyframe(type)};
  }
`;

export const TextContainer = styled.div`
  flex-wrap: wrap;
  overflow-wrap: break-word;
  word-wrap: break-word;
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  span {
    line-height: 27px;
    font-size: 14px;
  }
`;
export const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
