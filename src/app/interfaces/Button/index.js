import React from 'react';
import PropTypes from 'prop-types';
import { ButtonContainer, TextContainer, IconContainer } from './styles';
import { FA } from '../FA';

const Button = ({
  text = false,
  size = '',
  type = '',
  icon = false,
  buttonProps = {},
  iconProps = {}
}) => (
  <ButtonContainer size={size} type={type} {...buttonProps}>
    {text && (
      <TextContainer>
        <span>{text}</span>
      </TextContainer>
    )}
    {icon && (
      <IconContainer>
        <FA icon={icon} {...iconProps} />
      </IconContainer>
    )}
  </ButtonContainer>
);

const sizes = {
  sm: 'sm',
  md: 'md',
  lg: 'lg'
};

const types = {
  alert: 'alert',
  danger: 'danger',
  dark: 'dark',
  error: 'error',
  info: 'info',
  success: 'success'
};

Button.propTypes = {
  text: PropTypes.string,
  size: PropTypes.oneOf(Object.keys(sizes)),
  type: PropTypes.oneOf(Object.keys(types)),
  buttonProps: PropTypes.any,
  iconProps: PropTypes.any,
  icon: PropTypes.any
};

export { Button, sizes as buttonSizes, types as buttonTypes };
