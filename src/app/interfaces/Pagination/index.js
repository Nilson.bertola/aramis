import React from 'react';
import { Main } from './styles';
import { Buttons } from './Buttons';
import { Selection } from './Selection';

const Pagination = ({ pages, page, actionPage, actionSelection, perPage }) => (
  <Main>
    <Selection actionSelection={actionSelection} perPage={perPage} />
    <Buttons pages={pages} page={page} actionPage={actionPage} />
  </Main>
);

export { Pagination };
