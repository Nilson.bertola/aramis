import styled from 'styled-components';

export const Main = styled.div.attrs({
  className: 'col-12'
})`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
