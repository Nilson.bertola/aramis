import React from 'react';
import {
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faAngleLeft,
  faAngleRight
} from '@fortawesome/free-solid-svg-icons';
import { ButtonHolder, Button } from './styles';
import { FA } from '../../FA';

const renderNumberButton = (number, pages, action, current = false) =>
  number >= 0 &&
  number <= pages && (
    <Button onClick={() => action(number)} current={current}>
      {number}
    </Button>
  );

const renderIconButton = (icon, action) => (
  <Button onClick={action}>
    <FA icon={icon} />
  </Button>
);

const Buttons = ({ pages, page, actionPage }) => (
  <ButtonHolder>
    {page - 2 >= 0 && renderIconButton(faAngleDoubleLeft, () => actionPage(0))}
    {page - 1 >= 0 && renderIconButton(faAngleLeft, () => actionPage(page - 1))}
    {renderNumberButton(page - 2, pages, actionPage)}
    {renderNumberButton(page - 1, pages, actionPage)}
    {renderNumberButton(page, pages, () => null, true)}
    {renderNumberButton(page + 1, pages, actionPage)}
    {renderNumberButton(page + 2, pages, actionPage)}
    {page + 1 <= pages && renderIconButton(faAngleRight, () => actionPage(page + 1))}
    {page + 2 <= pages && renderIconButton(faAngleDoubleRight, () => actionPage(pages))}
  </ButtonHolder>
);

export { Buttons };
