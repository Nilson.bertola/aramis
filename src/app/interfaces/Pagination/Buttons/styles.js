import styled, { css } from 'styled-components';

export const ButtonHolder = styled.div`
  padding: 0px;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  flex-direction: row;
`;

const currentPage = css`
  background-color: #1db954;
  color: #eee;
  box-shadow: 1px 1px 10px #bbbd;
  cursor: default;
`;

const clickablePage = css`
  color: #1db954;
  cursor: pointer;
`;

export const Button = styled.div`
  ${({ current }) => (current ? currentPage : clickablePage)}
  padding: 8px 6px;
  margin: 2px 1px;
  border-radius: 3px;
`;
