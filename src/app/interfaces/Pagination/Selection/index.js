import React, { useState, useRef, useEffect } from 'react';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';
import { SelectDropDown, Selected, OptionsDropDown, Option } from './styles';
import { FA } from '../../FA';

const Selection = ({ perPage, actionSelection, options = [5, 10, 15, 20, 25] }) => {
  const [open, setOpen] = useState(false);
  const selectRef = useRef();

  const handleClick = e => {
    if (selectRef.current && !selectRef.current.contains(e.target)) {
      setOpen(false);
      return;
    }
  };

  useEffect(() => {
    // add when mounted
    document.addEventListener('mousedown', handleClick); // return function to be called when unmounted
    return () => {
      document.removeEventListener('mousedown', handleClick);
    };
  }, []);

  const handleOption = value => {
    actionSelection(value);
    setOpen(false);
  };

  return (
    <div ref={selectRef}>
      <SelectDropDown onClick={() => setOpen(!open)} open={open}>
        <Selected>{perPage}</Selected>
        <FA icon={faAngleDoubleDown} />
      </SelectDropDown>
      {open && (
        <OptionsDropDown>
          {options.map((op, i) => (
            <Option key={i} onClick={() => handleOption(op)}>
              {op}
            </Option>
          ))}
        </OptionsDropDown>
      )}
    </div>
  );
};

export { Selection };
