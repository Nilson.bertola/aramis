import styled, { keyframes, css } from 'styled-components';

const spin = 180 * 5;

const rotateSvg = keyframes`
  0% {
    transform:rotate(0deg);
  }
  100% {
    transform:rotate(${spin}deg);
  }
`;

const svgOpen = css`
  svg {
    animation: 0.6s ease-in-out forwards ${rotateSvg};
  }
`;

export const SelectDropDown = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-color: #1db954;
  color: #eee;
  box-shadow: 1px 1px 10px #bbbd;
  cursor: pointer;
  padding: 8px 6px;
  margin: 2px 1px;
  border-radius: 3px;

  ${({ open }) => (open ? svgOpen : '')}
`;

export const Selected = styled.span`
  margin: 0px 10px;
  font-size: 20px;
  font-family: 'Carter One', cursive;
`;

export const OptionsDropDown = styled.div`
  position: absolute;
  float: unset;
  z-index: 2;
`;

const optionAnimation = keyframes`
    0% {
        background-color: #eee;
        color: #1db954;
    }
    10%, 30%, 50%, 70%, 90% {
        transform: translateX(3px);
    }
    20%, 40%, 60%, 80% {
        transform: translateX(-3px);
    }
    100% {
        color: #eee;
        background-color: #1db954;
    }
`;

export const Option = styled.div`
  background-color: #eee;
  color: #1db954;
  cursor: pointer;
  padding: 8px 6px;
  width: 150%;
  :hover {
    animation: 0.5s forwards ease-in-out ${optionAnimation};
  }
`;
