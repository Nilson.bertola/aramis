if (!global._babelPolyfill) {
  require('babel-polyfill');
}
require('dotenv').config();
require('dotenv-safe').config();

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const path = require('path');
const { NODE_ENV, PORT, MONGO_STRING_CONN } = require('#config');
const withAuth = require('./api/middleware/withAuth');
const port = PORT || 3000;

// CONNECT TO MONGO
mongoose.connect(MONGO_STRING_CONN, { useNewUrlParser: true, useCreateIndex: true }, function(err) {
  if (err) {
    console.log(err);
  } else {
    console.log(`Successfully connected to the leaf ~blink~`);
  }
});

const app = express();

// SERVER PRESETS
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

// CONFIG API ROUTES
const apiRouter = express.Router();
require('./api/routes').map(i => i(apiRouter, withAuth));
app.use('/api', apiRouter);

// GOING DEV MODE !!!
if (NODE_ENV === 'development') {
  const webpack = require('webpack');
  const config = require('../webpack.config.js');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');

  config.entry.app.unshift('webpack-hot-middleware/client?reload=true&timeout=1000');
  config.plugins.push(new webpack.HotModuleReplacementPlugin());

  const compiler = webpack(config);

  app.use(
    webpackDevMiddleware(compiler, {
      publicPath: config.output.publicPath
    })
  );

  app.use(webpackHotMiddleware(compiler));
}

// STATIC APP ROUTE
const outputPath = path.resolve(__dirname, '../dist');
app.use('/', express.static(outputPath));
app.get('*', (req, res) => {
  res.sendFile(path.resolve(outputPath, 'index.html'));
});

// START SERVER
app.listen(port, () => console.info(`Magic happens on port ${port}`));
