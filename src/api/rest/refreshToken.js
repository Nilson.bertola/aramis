import { encodedIdSecret, objToFormData, refreshUrl as url } from './commons';
const { getAuth } = require('#firebase');
const fun = () =>
  new Promise(async resolve =>
    resolve({
      url,
      method: 'POST',
      data: objToFormData({
        grant_type: 'refresh_token',
        refresh_token: (await getAuth()).refresh_token
      }),
      headers: {
        Authorization: `Basic ${encodedIdSecret}`
      }
    })
  );

module.exports = async () => ({
  opt: await fun(),
  fun
});
