const { SPOTIFY_CLIENTID, SPOTIFY_CLIENTSECRET, SPOTIFY_REFRESH_TOKEN_URL } = require('#config');

const url = {
  refreshUrl: SPOTIFY_REFRESH_TOKEN_URL
};

const encodedIdSecret = new Buffer.from(`${SPOTIFY_CLIENTID}:${SPOTIFY_CLIENTSECRET}`).toString(
  'base64'
);

const objToFormData = obj =>
  Object.keys(obj)
    .map(k => `${k}=${obj[k]}`)
    .join('&');

module.exports = {
  encodedIdSecret,
  objToFormData,
  ...url
};
