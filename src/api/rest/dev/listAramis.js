const { getAuth } = require('#firebase');
const fun = (url = 'https://api.spotify.com/v1/playlists/5GIFsNTlSBcDtxqyNBnSEt/tracks') =>
  new Promise(async resolve =>
    resolve({
      opt: {
        url: url,
        method: 'get',
        headers: {
          Authorization: `Bearer ${(await getAuth()).access_token}`
        }
      },
      fun: fun.bind(url)
    })
  );

module.exports = fun;
