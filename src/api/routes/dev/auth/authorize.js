// Import our User schema
const { User } = require('#mongoModels');
const jwt = require('jsonwebtoken');
const { randomBytes } = require('crypto');
const { setRefreshed } = require('#helpers').auth.refreshTokens;
const { hours, days } = require('#helpers').time;
const { SECRET } = require('#config');

module.exports = router => {
  router.post('/dev/auth/authorize', async (req, res) => {
    const { email, password } = req.body;
    User.findOne({ email }, function(err, user) {
      if (err) {
        res.status(500).json({
          error: 'Internal error please try again'
        });
      } else if (!user) {
        res.status(401).json({
          error: 'Incorrect email',
          errCode: 'loginEmailNotFount'
        });
      } else {
        user.isCorrectPassword(password, function(err, same) {
          if (err) {
            res.status(500).json({
              error: 'Internal error please try again'
            });
          } else if (!same) {
            res.status(401).json({
              error: 'Incorrect email or password',
              errCode: 'loginWrongPassword'
            });
          } else {
            const refreshToken = randomBytes(512).toString('hex');
            const { verified, name, _id, createdAt, updatedAt } = user._doc;
            const payload = { email, verified, name, _id, createdAt, updatedAt, refreshToken };

            setRefreshed(payload);

            const token = jwt.sign(payload, SECRET, {
              expiresIn: hours(2)
            });
            res
              .cookie('xApiAramisToken', token, {
                expires: new Date(Date.now() + days(2))
              })
              .status(200)
              .json(payload);
          }
        });
      }
    });
  });
};
