// Import our User schema
const jwt = require('jsonwebtoken');
const { randomBytes } = require('crypto');
const { setRefreshed, getRefreshed, rejectToken } = require('#helpers').auth.refreshTokens;
const { hours, days } = require('#helpers').time;
const { SECRET } = require('#config');

const decodeToken = token => {
  try {
    return jwt.decode(token, SECRET);
  } catch (error) {
    return error;
  }
};

module.exports = router => {
  router.get('/dev/auth/refresh', async (req, res) => {
    const incommingToken = req.cookies.xApiAramisToken || null;

    const payload = decodeToken(incommingToken);
    const { refreshToken, email } = payload;

    if (getRefreshed({ refreshToken }) === email) {
      rejectToken({ refreshToken });

      const newRefreshToken = randomBytes(512).toString('hex');

      payload.refreshToken = newRefreshToken;
      delete payload.exp;
      delete payload.iat;

      setRefreshed(payload);

      const token = jwt.sign(payload, SECRET, {
        expiresIn: hours(2)
      });

      res
        .cookie('xApiAramisToken', token, {
          expires: new Date(Date.now() + days(2))
        })
        .sendStatus(200);
    } else {
      res.status(500).send('Internal Error');
    }
  });
};
