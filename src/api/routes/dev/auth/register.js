// Import our User schema
const { User } = require('#mongoModels');

module.exports = router => {
  router.post('/dev/auth/register', async (req, res) => {
    const { email, password } = req.body;
    const user = new User({ email, password });
    user.save(function(err) {
      if (err) {
        const message = 'Error registering new user please try again.';
        console.log(err);
        const parsedError = { ...getError(err), message };
        res.status(500).send(parsedError);
      } else {
        res.status(200).send('Welcome to the club!');
      }
    });
  });
};

const getError = error => {
  switch (error.code) {
    case 11000:
      return { error, errorParsed: 'userEmailDuplication' };

    default:
      return { error };
  }
};
