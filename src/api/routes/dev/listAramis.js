const { listAramis } = require('#rest').dev;
const { request } = require('#helpers');
const flattenDeep = require('lodash/flattenDeep');

module.exports = router => {
  router.get('/dev/aramis/list', async (req, res) => {
    const data = await request(await listAramis());
    const parsedData = parseData(data).sort((a, b) => b.popularity - a.popularity);
    res.status(200).send(parsedData);
  });
};

const parseData = data =>
  flattenDeep(
    data.map(({ items }) =>
      items.map(({ track }) => {
        // eslint-disable-next-line
        const { external_urls, name, popularity, album, artists } = track;
        const img = album.images[0].url;
        const artist = artists[0].name;
        const href = external_urls.spotify;
        return {
          href,
          name,
          popularity,
          img,
          artist
        };
      })
    )
  );
