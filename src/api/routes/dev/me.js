const { me } = require('#rest').dev;
const { request } = require('#helpers');

module.exports = router => {
  router.get('/dev/aramis/me', async (req, res) => {
    const data = await request(await me());
    const parsedData = parseData(data);
    res.status(200).send(parsedData);
  });
};

const parseData = data => {
  const {
    country,
    display_name: name,
    email,
    external_urls: externalUrl,
    followers,
    images,
    product
  } = data[0];
  const href = externalUrl.spotify;
  const totalFollowers = followers.total;
  const img = images[0].url;
  const premium = product == 'premium';
  return {
    country,
    name,
    email,
    href,
    totalFollowers,
    img,
    premium
  };
};
