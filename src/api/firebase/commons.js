/* eslint-disable camelcase */
const firebase = require('firebase-admin');
const {
  FIREBASE_DATABASEURL: databaseURL,
  FIREBASE_TYPE: type,
  FIREBASE_PROJECT_ID: project_id,
  FIREBASE_PRIVATE_KEY_ID: private_key_id,
  FIREBASE_PRIVATE_KEY: private_key,
  FIREBASE_CLIENT_EMAIL: client_email,
  FIREBASE_CLIENT_ID: client_id,
  FIREBASE_AUTH_URI: auth_uri,
  FIREBASE_TOKEN_URI: token_uri,
  FIREBASE_AUTH_PROVIDER_X509_CERT_URL: auth_provider_x509_cert_url,
  FIREBASE_CLIENT_X509_CERT_URL: client_x509_cert_url
} = require('#config');

const config = {
  type,
  project_id,
  private_key_id,
  private_key: private_key.replace(/\\n/g, '\n'),
  client_email,
  client_id,
  auth_uri,
  token_uri,
  auth_provider_x509_cert_url,
  client_x509_cert_url
};

firebase.initializeApp({
  credential: firebase.credential.cert(config),
  databaseURL
});

module.exports = { firebase };
