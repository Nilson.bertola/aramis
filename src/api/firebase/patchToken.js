const { firebase } = require('./commons');

module.exports = patchedAuth =>
  new Promise(async resolve => {
    firebase
      .database()
      .ref('auth')
      .update(patchedAuth, er => {
        resolve(er === null);
      });
  });
