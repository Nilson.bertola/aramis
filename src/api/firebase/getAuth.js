const { firebase } = require('./commons');
module.exports = () =>
  new Promise(resolve => {
    firebase
      .database()
      .ref('auth')
      .once('value', snap => resolve(snap.val()));
  });
