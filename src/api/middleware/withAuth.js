const jwt = require('jsonwebtoken');
const { SECRET, NODE_ENV } = require('#config');

module.exports = (req, res, next) => {
  const token = req.cookies.xApiAramisToken || null;

  if (!token) {
    res.status(401).send('Unauthorized: No token provided');
  } else {
    jwt.verify(token, SECRET, function(err, decoded) {
      if (err) {
        res.status(401).json(err);
      } else {
        req.email = decoded.email;
        next();
      }
    });
  }
};
