/* eslint-disable no-invalid-this */
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const schema = {
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  verified: { type: Boolean, default: false, required: true },
  fName: { type: String, required: false, default: '' },
  lName: { type: String, required: false, default: '' }
};

const options = { timestamps: true };

/**
 *  hash a password on insert or update password
 * @param {function} next
 */
function crypMiddleware(next) {
  if (this.isNew || this.isModified('password')) {
    const document = this;
    bcrypt.hash(document.password, saltRounds, function(err, hashedPassword) {
      if (err) {
        next(err);
      } else {
        document.password = hashedPassword;
        next();
      }
    });
  } else {
    next();
  }
}

/**
 *  compare hashed password key
 * @param {string} password
 * @param {function} callback
 */
function isCorrectPassword(password, callback) {
  bcrypt.compare(password, this.password, function(err, same) {
    if (err) {
      callback(err);
    } else {
      callback(err, same);
    }
  });
}

const UserSchema = new mongoose.Schema(schema, options);

UserSchema.methods = { isCorrectPassword };

UserSchema.pre('save', crypMiddleware);

module.exports = mongoose.model('User', UserSchema);
