const tokens = {};
const setRefreshed = ({ refreshToken, email }) => {
  tokens[refreshToken] = email;
};
const getRefreshed = ({ refreshToken }) => tokens[refreshToken];
const rejectToken = ({ refreshToken }) => {
  delete tokens[refreshToken];
};
module.exports = { tokens, setRefreshed, getRefreshed, rejectToken };
