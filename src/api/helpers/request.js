import axios from 'axios';
import { refreshToken } from '#rest';
import { patchToken } from '#firebase';

const request = ({ opt, fun }, flag = 'request') =>
  new Promise(async (resolve, reject) => {
    axios(opt)
      .then(async ({ data }) => {
        if (flag === 'refresh') {
          /* eslint-disable camelcase */
          const { access_token } = data;
          const patch = { access_token };
          return await patchToken(patch);
          /* eslint-enable camelcase */
        }

        let response = data;

        let next = [];
        if (response.next) next = [...(await request(await fun(response.next), 'next'))];
        response = [response, ...next];
        if (flag === 'next') return resolve(response);
        resolve(response);
      })
      .then(data => {
        resolve(data);
      })
      .catch(async ({ response }) => {
        const { status } = response;
        if (status === 401) {
          await request(await refreshToken(), 'refresh');
          const data = await request(await fun(), 'request');
          resolve(data);
        }
      });
  });

module.exports = request;
