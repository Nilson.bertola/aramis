const seconds = (s = 1) => s * 1000;
const minutes = (m = 1) => m * seconds() * 60;
const hours = (h = 1) => h * minutes() * 60;
const days = (d = 1) => d * hours() * 24;

module.exports = {
  seconds,
  minutes,
  hours,
  days
};
