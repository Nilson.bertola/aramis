require('dotenv').config();
require('dotenv-safe').config();
const { NODE_ENV } = require('./config');

const DotEnv = require('dotenv-webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const path = require('path');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

const moment = new MomentLocalesPlugin({
  localesToKeep: ['pt-BR']
});

const htmlPlugin = {
  production: new HtmlWebPackPlugin({
    template: './src/app/index.html',
    minify: {
      removeComments: true,
      collapseWhitespace: true,
      removeRedundantAttributes: true,
      useShortDoctype: true,
      removeEmptyAttributes: true,
      removeStyleLinkTypeAttributes: true,
      keepClosingSlash: true,
      minifyJS: true,
      minifyCSS: true,
      minifyURLs: true
    },
    inject: true
  }),
  development: new HtmlWebPackPlugin({
    template: './src/app/index.html'
  })
};

const env = new DotEnv();

const optimization = {
  production: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          warnings: false,
          compress: {
            comparisons: false
          },
          parse: {},
          mangle: true,
          output: {
            comments: false,
            ascii_only: true
          }
        },
        parallel: true,
        cache: true,
        sourceMap: true
      })
    ],
    nodeEnv: 'production',
    sideEffects: true,
    concatenateModules: true,
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 10,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
            return `npm.${packageName.replace('@', '')}`;
          }
        }
      }
    }
  },
  development: {
    minimize: false
  }
};

module.exports = {
  watch: false,
  mode: 'production',
  entry: {
    app: ['./src/app/index.js']
  },
  output: {
    path: path.join(__dirname, './dist'),
    filename: '[name].js'
  },
  plugins: [htmlPlugin[NODE_ENV], env, moment],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.s?css$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'file-loader'
      },
      {
        test: /\.ttf$/,
        loader: 'ttf-loader'
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader'
          },
          {
            loader: 'react-svg-loader',
            options: {
              jsx: true // true outputs JSX tags
            }
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      // APP ALIAS
      '#store': path.resolve(__dirname, './src/app/store'),
      '#components': path.resolve(__dirname, './src/app/components'),
      '#interfaces': path.resolve(__dirname, './src/app/interfaces'),
      '#assets': path.resolve(__dirname, './src/app/assets'),
      // API ALIAS
      '#loaders': path.resolve(__dirname, './loaders'),
      '#config': path.resolve(__dirname, './config'),
      '#helpers': path.resolve(__dirname, './src/api/helpers'),
      '#rest': path.resolve(__dirname, './src/api/rest'),
      '#firebase': path.resolve(__dirname, './src/api/firebase'),
      '#mongoModels': path.resolve(__dirname, './src/api/mongo/models')
    }
  },
  optimization: optimization[NODE_ENV]
};
